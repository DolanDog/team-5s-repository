﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanceScript : MonoBehaviour
{
    public EnemyController enemyController;
    public Jumper jumper;

    private float[] attackDetails = new float[2];
    private Rigidbody2D rb;
    private float lastDash = -100f;
    private float dashTimeRemaining;
    public bool isDashing;
    public bool dashStopped;

    public float lanceDamage = 10.0f;
    public float attackRange = 0.5f;
    public LayerMask enemyLayer;
    public Transform attackPoint;
    public int facingDirection;

    public float dashTime;
    public float dashSpeed;
    public float dashCooldown;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        facingDirection = GameObject.Find("Jumper").GetComponent<Jumper>().facingDirection;
        checkInput();
        CheckDash();
    }

    void checkInput()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (Time.time >= (lastDash + dashCooldown))
            {
                AttemptToDash();
                Attack();
            }
        }
    }

    private void AttemptToDash()
    {
        isDashing = true;
        dashTimeRemaining = dashTime;
        lastDash = Time.time;
        dashStopped = false;
    }

    private void CheckDash()
    {

        if (isDashing)
        {
            rb.velocity = new Vector2(dashSpeed * facingDirection, 0.0f);
            dashTimeRemaining -= Time.deltaTime;
        }
        if (dashTimeRemaining <= 0)
        {
            isDashing = false;
            if (dashStopped == false)
            {
                stopdash();
            }
        }
    }

    private void stopdash ()
    {
        rb.velocity = Vector3.zero;
        dashStopped = true;
    }

    void Attack()
    {
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayer);

        foreach(Collider2D Enemies in hitEnemies)
        {
            attackDetails[0] = lanceDamage;
            attackDetails[1] = transform.position.x;
            Enemies.transform.parent.SendMessage("Damage", attackDetails);
            Debug.Log("We hit");
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }
}

