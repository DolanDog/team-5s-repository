﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezeFrame : MonoBehaviour
{
    public int freezeFrames = 10;
    public int leftToFreeze;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            leftToFreeze = freezeFrames;
        }

        if (leftToFreeze > 0)
        {
            leftToFreeze--;
            Time.timeScale = 0.0f;
        }
        else
        {
            Time.timeScale = 1.0f;
        }
    }
}
