﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]

public class ArcThrow : MonoBehaviour
{

    public GameObject throwobject;

    void Start()
    {
        throwobject.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            throwobject.SetActive(true);
            // animator.SetBool("TestTube", true);
            TestTubeThrow();
            //speed = Mathf.Lerp(10, 0, Time.deltaTime);
            // throwobject.transform.Translate(Vector3.forward * 10);
            //StartCoroutine(GrenadeCooldown());
        }

    }

    void TestTubeThrow()
    {
        //StartCoroutine(COPlayOneShot("TestTube"));
        Instantiate(throwobject, new Vector3(10 * 2.0F, 0, 0), Quaternion.identity);
    }
}

/*    it explodes in 10seconds if it is'nt thrown
        IEnumerator TestTubeCooldown()
        {
            canFire = false;
            yield return new WaitForSeconds(0.01f);
            //rifleMuzzle.GetComponent<ParticleSystem>().top();
            canFire = true;
            animator.SetBool("TestTube",false);
        }
}
*/